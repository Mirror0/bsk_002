package com.company;

import java.util.ArrayList;
import java.util.Collections;

public class LFSR {

    public int _degree;
    private byte[] _seed;
    private byte[] _polimonial;
    private byte[] regs;
    private byte[] bits;
    private int t = 0;
    private int bitCounter = 0;
    public static boolean first = true;

    LFSR(String polimonial) {
        //k = new byte[8];
        String[] xs = polimonial.split("\\+");
        String[] tmp;
        ArrayList<Integer> poli = new ArrayList<Integer>();
        for (String x : xs) {
            tmp = x.split("\\^");
            if (tmp.length == 2) {
                poli.add(Integer.parseInt(tmp[1].trim()));
            } else if (tmp.length == 1 && (tmp[0].trim().equals("x") || tmp[0].trim().equals("X"))) {
                poli.add(1);
            }
        }
        Collections.sort(poli);
        Collections.reverse(poli);

        _degree = poli.get(0);

        _polimonial = new byte[_degree];

        for (Integer i : poli) {
            _polimonial[_polimonial.length - i.intValue()] = 1;
        }

        //_seed = seed;

        regs = new byte[_degree];
    }

    public void Init(byte[] seed) {
        _seed = seed;
        for (int i = 0; i < _seed.length; i++) {
            regs[i] = _seed[i];
        }
    }

    private void Init() {
        for (int i = 0; i < _seed.length; i++) {
            regs[i] = _seed[i];
        }
    }

    private byte GenerateFeed() {
        Byte feed = null;

        for (int i = 0; i < _polimonial.length; i++) {
            if (_polimonial[i] == 1) {
                if (feed == null) {
                    feed = regs[i];
                } else {
                    feed = (byte) (feed ^ regs[i]);
                }
            }
        }
        if(first) {
            System.out.print("#" + (t++) + " -> ");
            for (int i = 0; i < regs.length; i++) {
                System.out.print(regs[i]);
            }
            System.out.println();
        }
        for (int j = regs.length - 1; j > 0; j--) {
            regs[j] = regs[j - 1];
        }
        regs[0] = feed;
        //k[f++%8] = feed;
        return feed;
    }

    private byte Generate8bitFeed()
    {
        byte[] tmp = new byte[8];
        byte res = 0;
        for(int i=0;i<tmp.length;i++)
        {
            tmp[i] = GenerateFeed();
            res += tmp[i]*Math.pow(2,i);
        }
        return res;
    }

    public byte[] Transform(byte[] data) {
       // Init();
        first = false;
        byte[] res = new byte[data.length];

        for (int i = 0; i < data.length; i++) {
            res[i] = (byte) (Generate8bitFeed() ^ data[i]);
        }

        return res;
    }

    private byte GenerateAutoKeyEncodeFeed()
    {
        Byte feed;
        //first = false;
        feed = GenerateFeed();
        feed = (byte) (feed ^ bits[bitCounter++]);
        regs[0] = feed;
        return feed;
    }

    private byte GenerateAutoKeyDecodeFeed()
    {
        Byte feed;
        //first = false;
        feed = GenerateFeed();
        regs[0] = bits[bitCounter++];
        return feed;
    }


    public byte[] EncodeAutoKey(byte[] data)
    {
        bits = ByteToBits(data);
        bitCounter = 0;
        byte[] res_bits = new byte[bits.length];

        byte[] res = new byte[data.length];

        for(int k=0;k<bits.length;k++)
        {
            res_bits[k] = GenerateAutoKeyEncodeFeed();
        }

        for(int l=0,m=0;l<bits.length;l=l+8,m++)
        {
            res[m] = (byte) ( res_bits[l+7]*Math.pow(2,(l)%8) +
                    res_bits[l+6]*Math.pow(2,(l+1)%8) +
                    res_bits[l+5]*Math.pow(2,(l+2)%8) +
                    res_bits[l+4]*Math.pow(2,(l+3)%8) +
                    res_bits[l+3]*Math.pow(2,(l+4)%8) +
                    res_bits[l+2]*Math.pow(2,(l+5)%8) +
                    res_bits[l+1]*Math.pow(2,(l+6)%8) +
                    res_bits[l]*Math.pow(2,(l+7)%8) );
        }

        return res;
    }

    public byte[] DecodeAutoKey(byte[] data)
    {
        bits = ByteToBits(data);
        bitCounter = 0;
        byte[] res_bits = new byte[bits.length];

        byte[] res = new byte[data.length];

        for(int k=0;k<bits.length;k++)
        {
            res_bits[k] = (byte) ( GenerateAutoKeyDecodeFeed() ^ bits[k] );
        }

        for(int l=0,m=0;l<bits.length;l=l+8,m++)
        {
            res[m] = (byte) ( res_bits[l+7]*Math.pow(2,(l)%8) +
                    res_bits[l+6]*Math.pow(2,(l+1)%8) +
                    res_bits[l+5]*Math.pow(2,(l+2)%8) +
                    res_bits[l+4]*Math.pow(2,(l+3)%8) +
                    res_bits[l+3]*Math.pow(2,(l+4)%8) +
                    res_bits[l+2]*Math.pow(2,(l+5)%8) +
                    res_bits[l+1]*Math.pow(2,(l+6)%8) +
                    res_bits[l]*Math.pow(2,(l+7)%8) );
        }

        return res;
    }

    private byte[] ByteToBits(byte[] data)
    {
        byte[] bits = new byte[data.length*8];
       // String str;
        int k=0;
       // Character tmp;
       // str = Integer.toBinaryString(data[0]);
        for(int i=0;i<data.length;i++)
        {
            for(int j=7; j>=0; j--,k++)
            {
                if(((1<<j)&data[i]) != 0)
                {
                    bits[k] = 1;
                }
                else
                {
                    bits[k] = 0;
                }
            }
        }

        //boolean t = true;
        //byte g = 1;
       // g = (byte) g^t;

        return bits;
    }


    public byte initialTransform(){
        //Init();
        //byte[] res = new byte[data.length];

            GenerateFeed();
            return regs[0];

    }
}
