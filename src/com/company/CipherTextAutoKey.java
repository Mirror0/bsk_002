package com.company;

import java.util.ArrayList;
import java.util.Collections;

public class CipherTextAutoKey {
    private static Integer _degree;
    private static byte[] _polimonial;
    private static byte[] _seed;
    private static byte[] regs;


    public CipherTextAutoKey() {

    }

    public static byte[] Encrypt(byte[] data, String polimonial, byte[] seed) {
        init(polimonial, seed);
        byte[] encryptedData;
        encryptedData = generateEncryptData(data);
        return encryptedData;
    }

    private static byte[] generateEncryptData(byte[] data) {
        Byte feed = null;
        byte[] result = new byte[data.length];
        for (int i = 0; i < data.length; i++) {
            feed = getFeed(feed);
            feed = (byte) (feed ^ data[i]);

            for (int j = regs.length - 1; j > 0; j--) {
                regs[j] = regs[j - 1];
            }
            regs[0] = feed;
            result[i] = feed;
            feed = null;
        }
        return result;
    }

    public static byte[] Decrypt(byte[] data, String polimonial, byte[] seed) {
        init(polimonial, seed);
        byte[] decryptedData;
        decryptedData = generateDecryptData(data);
        return decryptedData;
    }

    private static byte[] generateDecryptData(byte[] data) {
        Byte feed = null;
        byte[] result = new byte[data.length];
        for (int i = 0; i < data.length; i++) {
            feed = getFeed(feed);

            for (int j = regs.length - 1; j > 0; j--) {
                regs[j] = regs[j - 1];
            }
            regs[0] = data[i];
            result[i] = (byte)(feed ^ data[i]);
            feed = null;
        }
        return result;
    }

    private static Byte getFeed(Byte feed) {
        for (int j = 0; j < _polimonial.length; j++) {
            if (_polimonial[j] == 1) {
                if (feed == null) {
                    feed = regs[j];
                } else {
                    feed = (byte) (feed ^ regs[j]);
                }
            }
        }
        return feed;
    }

    private static void init(String polimonial, byte[] seed) {
        String[] xs = polimonial.split("\\+");
        String[] tmp;
        ArrayList<Integer> poli = new ArrayList<Integer>();
        for (String x : xs) {
            tmp = x.split("\\^");
            if (tmp.length == 2) {
                poli.add(Integer.parseInt(tmp[1].trim()));
            } else if (tmp.length == 1 && (tmp[0].trim().equals("x") || tmp[0].trim().equals("X"))) {
                poli.add(1);
            }
        }
        Collections.sort(poli);
        Collections.reverse(poli);

        _degree = poli.get(0);

        _polimonial = new byte[_degree];

        for (Integer i : poli) {
            _polimonial[_polimonial.length - i.intValue()] = 1;
        }
        _seed = seed;
        regs = new byte[_degree];
        _seed = seed;
        for (int i = 0; i < _seed.length; i++) {
            regs[i] = _seed[i];
        }
    }
}
