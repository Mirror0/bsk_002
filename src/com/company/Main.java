package com.company;

import java.io.IOException;
import java.nio.file.*;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    private static byte[] Read(String file) throws IOException {
        return Files.readAllBytes(Paths.get(file));
    }

    private static String toString(byte[] bytes) {
        String res = "";
        for (int i = 0; i < 200; i++) {
            res += bytes[i];
        }
        res += " +" + (bytes.length - 200) + " bits";
        return res;
    }

    private static String toString1(byte[] bytes)
    {
        String res = "";
        for (int i = 0; i < 100; i++) {
            res += bytes[i];
        }
        //res += " +" + (bytes.length - 200) + " bits";
        return res;
    }

    public static void main(String[] args) throws IOException {

        while (true) {
            System.out.println("1 lub 2 lub 3");
            Scanner scanner = new Scanner(System.in);
            Integer choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    lfsrFirstMethod();
                    break;
                case 2:
                    lfsrMethod();
                    break;
                case 3:
                    ctkaMethod();
                    break;
                default:
                    break;
            }
        }
    }

    private static void lfsrFirstMethod() throws IOException {
        com.company.LFSR.first = true;
       // byte[] data = Read("test.bin");
        byte[] seed;// = {1, 1, 0, 1};
        byte[] transform = new byte[100];
        String function;
        Scanner stringScanner = new Scanner(System.in);
        Scanner sc = new Scanner(System.in);
        System.out.println("gimme wielomian");
        function = stringScanner.nextLine();
        LFSR lfsr = new LFSR(function);
        seed = new byte[lfsr._degree];
        System.out.println("gimme seed");
        for(int i=0;i<lfsr._degree;i++)
        {
            System.out.println("#"+i);
            seed[i] = sc.nextByte();
        }
        lfsr.Init(seed);
        for(int i=0;i<100;i++)
        {
            transform[i] = lfsr.initialTransform();
        }
        //transform = lfsr.initialTransform(data);
       // System.out.println("Data : \t " + toString(data));
        System.out.println("Trans: \t " + toString1(transform));
        com.company.LFSR.first = false;
    }


    private static void lfsrMethod() throws IOException {
        Scanner stringScanner = new Scanner(System.in);
        Scanner sc = new Scanner(System.in);
        Scanner sc1 = new Scanner(System.in);
        String file;
        byte[] data;
        byte[] seed = {};//= {1, 1, 0, 1};
        byte[] encrypt;
        byte[] decrypt;
        String function;
        LFSR lfsr;
        System.out.println("1: encrypt\n2: decrypt");
        Integer ch = sc.nextInt();

        switch (ch) {
            case 1:
                System.out.println("gimme wielomian");
                function = stringScanner.nextLine();
                lfsr = new LFSR(function);
                seed = new byte[lfsr._degree];
                System.out.println("gimme seed");
                for(int i=0;i<lfsr._degree;i++)
                {
                    System.out.println("#"+i);
                    seed[i] = sc1.nextByte();
                }
                lfsr.Init(seed);
                System.out.println("Path to read file");
                file = stringScanner.nextLine();
                data = Read(file);
                encrypt = lfsr.Transform(data);
                System.out.println("Path to write file");
                file = stringScanner.nextLine();
                Files.write(Paths.get(file), encrypt);
                System.out.println("Data: " + toString(data));
                System.out.println("Enc:  " + toString(encrypt));
                break;
            case 2:
                System.out.println("gimme wielomian");
                function = stringScanner.nextLine();
                lfsr = new LFSR(function);
                seed = new byte[lfsr._degree];
                System.out.println("gimme seed");
                for(int i=0;i<lfsr._degree;i++)
                {
                    System.out.println("#"+i);
                    seed[i] = sc1.nextByte();
                }
                lfsr.Init(seed);
                System.out.println("Path to read file");
                file = stringScanner.nextLine();
                encrypt = Read(file);
                System.out.println("Path to write file");
                Scanner out = new Scanner(System.in);
                file = out.nextLine();
                decrypt = lfsr.Transform(encrypt);
                Files.write(Paths.get(file), decrypt);
                System.out.println("Data: " + toString(encrypt));
                System.out.println("Dec:  " + toString(decrypt));
                break;
        }
    }

    private static void ctkaMethod() throws IOException {
        Scanner stringScanner = new Scanner(System.in);
        System.out.println("Path to encrypt file");
        String file;
        String function;
        LFSR lfsr;
        byte[] data;
        byte[] seed = {};
        byte[] encrypt;
        byte[] decrypt;
        System.out.println("1: encrypt\n2: decrypt");
        Scanner sc = new Scanner(System.in);
        Integer ch = sc.nextInt();
        switch (ch) {
            case 1:
                System.out.println("gimme wielomian");
                function = stringScanner.nextLine();
                lfsr = new LFSR(function);
                seed = new byte[lfsr._degree];
                System.out.println("gimme seed");
                for(int i=0;i<lfsr._degree;i++)
                {
                    System.out.println("#"+i);
                    seed[i] = sc.nextByte();
                }
                lfsr.Init(seed);
                System.out.println("Path to read file");
                file = stringScanner.nextLine();
                data = Read(file);
                encrypt = lfsr.EncodeAutoKey(data);
                System.out.println("Path to write file");
                file = stringScanner.nextLine();
                Files.write(Paths.get(file), encrypt);
                System.out.println("Data: " + toString(data));
                System.out.println("Enc:  " + toString(encrypt));
                break;
            case 2:
                System.out.println("gimme wielomian");
                function = stringScanner.nextLine();
                lfsr = new LFSR(function);
                seed = new byte[lfsr._degree];
                System.out.println("gimme seed");
                for(int i=0;i<lfsr._degree;i++)
                {
                    System.out.println("#"+i);
                    seed[i] = sc.nextByte();
                }
                lfsr.Init(seed);
                System.out.println("Path to read file");
                file = stringScanner.nextLine();
                encrypt = Read(file);
                System.out.println("Path to write file");
                Scanner out = new Scanner(System.in);
                file = out.nextLine();
                decrypt = lfsr.DecodeAutoKey(encrypt);
                Files.write(Paths.get(file), decrypt);
                System.out.println("Data: " + toString(encrypt));
                System.out.println("Dec:  " + toString(decrypt));
                break;
        }
    }
}
